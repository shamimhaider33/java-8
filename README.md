
#Why Java 8 ?
	-	More concise code and simpler use of multi-core processors
	-	As commodity CPUs have become multi-core, Difficult to take advantage with traditional threads programming.
	-	Java 5 added building blocks like thread pools and concurrent collections
	-	Java 7 added the fork/join framework, making parallelism more practical but still difficult

#What most important to us:
	-	Interface evolution
	-	Lambda expression
	-	Techniques for passing code to methods (Behaviour Parameterization)
	-	Streams API
	-	Parallel stream processing and performance
	-	Improvement date/time api
	
#Calculating prime Number b/w 1 to 1_000_000 :
	class Prime{
		public static boolean isPrime(Long n){
			boolean isPrimeNumber=true;
			for(int i=2;i<n;i++){
				if(n%i==0)
					isPrimeNumber=false;
			}
			return isPrimeNumber;
		}
	}

	System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "8");
	or
	java -Djava.util.concurrent.ForkJoinPool.common.parallelism=8 GetParallelism

#Functional Interface? :
	-	Java 8 introduced a new FunctionalInterface annotation type that lets you annotate an interface as being functional.
	-	A functional interface can define as many default and static methods as it requires but it must declare exactly one abstract method, or the compiler will complain that it isn't a functional interface.
	-	Readable, Callable, Iterable, Closeable, Flushable, Formattable, Comparable, Comparator 
	
	@FunctionalInterface
	public interface Runnable {
		public abstract void run();
	}

#Methods vs. Functions :

Method :

	-	A method mutates , i.e., it modifies data and produces other changes and side effects.
	-	Methods describe how things are done.
	-	Methods are used imperatively
	-	Order of invocation matters.
	-	Methods are code , i.e.they are invoked and executed

	Collections.sort(list, new Comparator<Student>() {
		@Override
		public int compare(Student o1, Student o2) {
			return 0;
		}
	});	


Function :

	-	A pure function does not mutate it just inspects data and produces results in form of new data.
	-	Pure functions describe what is done rather than how it is done.
	-	Pure functions are used declaratively
	-	Functions are code-as-data they are not only executable,but are also passed around like data.

#Interface evolution :

Problem with java-7 and prior interfaces?

	-	You provide abstract method in the interface, if 10 method is declare then implementation class need to provide implementation to all 10 methods.
	-	If we need to change the interface ( ie add new method prototype) the all the class hierarchy is get effected!

Solution provided by Java 8.

	-	Java 8 solve these issues by supporting default implementation and static method to interface.
	
	public interface Addressable	{
	  	String getStreet();
		String getCity();
		
		default String getFullAddress()	{
     		 return getStreet()+", "+getCity();
     	}
	}

	-	Now we are free not to implement getFullAddess() method, We can override if it is required, Even we can re-declare it as an abstract method in an abstract class.

Default methods use cases :

	-	Evolving existing interfaces
	
			To implement the new Streams API, it was necessary to evolve the Collections Framework's java.util.Collection interface by adding new default Stream<E> stream() and default Stream<E> parallelStream() methods. 
			Without default methods, Collection implementers such as the java.util.	ArrayList class would have been forced to implement these new methods or break source/binary compatibility.

	-	Increasing design flexibility
	
			Abstract classes have traditionally been used to share functionality between various concrete subclasses. However, single-class extension has limited this design choice.			
			Default methods offer greater flexibility because you can implement an interface at any point in the class hierarchy and access the interface's default methods from the implementing classes.
			Also, it's no longer necessary to create adapter classes for multi-method event listener interfaces. Instead, you can add a default method for each listener method to the event listener interface and override these methods as necessary.

	Note : We cannot use default methods to override any of the non-final methods in the java.lang.Object class. So it will not work:
		
		public interface Foo {
			default boolean equals(Object o) {
      			return false;
      		}
		}

Static method inside interface:
 
	-	In Java 8 a static methods be defined inside interfaces mainly to assist default methods.
	
			static <T> Comparator<T> comparingDouble(ToDoubleFunction<? super T> keyExtractor)
	
	-	As well as being directly invocable, comparingDouble() is invoked from this default method of Comparator.
		
			default Comparator<T> thenComparingDouble(ToDoubleFunction<? super T> keyExtractor)

	Note : When you implement an interface that contains a static method, the static method is still part of the interface and not part of the implementing class. For this reason, you cannot prefix the method with the class name. Instead, you must prefix the method with the interface name.

	interface X {
	  static void foo()	{
     	System.out.println("foo");
      }
	}
 
	class Y implements X {
	}
 
	public class Z {
	  public static void main(String[] args) 	{
     	 X.foo();
     	// Y.foo(); // won't compile
     }
	}

	Expression Y.foo() will not compile because foo() is a static member of interface X and not a static member of class Y.

Issues with multiple inheritance :

	-	Any class win over interface, If there is a method with a body or declaration	in the superclass chain, we ignore the interface completely.
	-	Subtype wins over Supertype
	-	If previous 2 rules don't give us the answer, the subclass must implements the method/declare it  abstract.

#Lambda Expressions & Functional Interfaces

What is lambda expression?

	-	Conceptually, a lambda expression is a function. It is an unnamed piece of reusable functionality.
	-	Anonymous Function : We call it as function as it is not associated with a particular class like a method is.
	-	Like a method a lambda has a list of parameters, a body, a return type and a possible list of exceptions that can be thrown.
	-	Passed around: A lambda expression can be passed around or can be stored in a variable.
	-	Concise: There is no need to write a lot of boilerplate like you do for anonymous inner class.

	public class LambdaDemo {
		public static void main(String[] args) {
    		Runnable r = new Runnable() {
       	      @Override
       	      public void run() {
               System.out.println("Running");
             }
           };
		Thread t=new Thread(r);
		
		Thread t2=new Thread(() -> System.out.println("Running"));
		}
	}


	interface Operation {
		int apply(int a, int b);
	}
	
	class Logic {
		public static int operation(int a, int b, Operation operation) {
			return operation.apply(a, b);
		}
	}
	
	public class DemoBehaviour {
	
		public static void main(String[] args) {
			int sum = Logic.operation(2, 2, (int a, int b) -> a + b);
			int mul = Logic.operation(2, 2, (int a, int b) -> a * b);
		}
	}

	Note : Associated with a lambda is an implicit target type (the type of object to which a lambda is bound). Because the target type must be a functional interface and is inferred from the context.

#Lambdas vs Anonymous Inner Classes
	Syntax :
			Anonymous inner class:
				File[] fs = myDir.listFiles(
								new FileFilter() {
    								public boolean accept(File f) { 
    									return f.isFile(); 
    								}
    							} 
    			);
    			
    		Lambda expression in various forms including method reference:
    			File[] files = myDir.listFiles((File f) -> {return f.isFile();});
    			File[] files = myDir.listFiles(f -> f.isFile());
    			File[] files = myDir.listFiles(File::isFile );

	Runtime Overhead :
			Lambda expressions have the potential for optimizations and reduced runtime overhead compared to anonymous inner classes.
			At runtime anonymous inner classes require:
				-	Class loading
				-	Memory allocation and object initialization
				-	Invocation of a non-static method

	Variable Binding :
			Variable capture: An inner class has access to all final variables of its enclosing context.
				void method() {
         			final int  cnt = 16;
         			Runnable r = new Runnable() {
            			public void run() {
                			System.out.println("count: " +  cnt );
            			}
        			};
        		Thread t = new Thread(r);
        		t.start(); 
           cnt ++;   // error: cnt is final
			}
			
			Lambda expressions, too,
				Runnable r = () -> { System.out.println("count: " +  cnt ); }; 
        		Thread t = new Thread(r);
        		t.start(); 
          	cnt ++;   // error: cnt is implicitly final
			}

	Difference:
		Variables from the enclosing context that are used inside a lambda expression are implicitly final (or effectively final)

#Passing code with behavior parameterization (Techniques for passing code to methods)
	-	Suppose you want to write two methods that differ in only a few lines of code; you can now just pass the code of the parts that differ as an argument.

#Behavior parameterization :
	-	Passing code to methods (and also being able to return it and incorporate it into data structures) also provides access to a whole range of additional techniques that are commonly referred to as functional-style programming.
	
Example :
		
		Farmer need to find out apple that are
			1. green in color
			2. having weight more then 150 gm
			
	public class Apple {
		private String color;
		private int weight;
	}
	
	public class Inventrory {
		public static List<Apple> getGreenApple(List<Apple> apples){
			List<Apple>geenApples=new ArrayList<Apple>();
			for(Apple temp:apples){
				 if(temp.getColor().equalsIgnoreCase("green"))
				 	geenApples.add(temp);
			}
			return geenApples;
		}
	
		public static List<Apple> getAppleswithMoreWeights(List<Apple> apples){
			List<Apple>applesWithMoreWt=new ArrayList<Apple>();
			for(Apple temp:apples){
				 if(temp.getWeight()>150)
					 applesWithMoreWt.add(temp);
			}
			return applesWithMoreWt;
		}	
	}

	Java 8 makes it possible to pass the code of the condition as an argument, thus avoiding code duplication of the filter method
	
	interface Predicate<T>{
		boolean test(T t);
	}
	
	public static List<Apple> filterApples(List<Apple> apples, Predicate<Apple>p){
		List<Apple> applesOnCondiation=new ArrayList<Apple>();
		for(Apple temp:apples){
			 if( p.test(temp))
				 applesOnCondiation.add(temp);
		}
		return applesOnCondiation;
	}
	
How to pass predicate?
	
	Traditional way? Using inner classes!	
		List<Apple>grrenApples=Inventrory2.filterApples(list, new Predicate<Apple>() {
			@Override
			public boolean test(Apple t) {
				return t.getColor().equals("green");
			}
		});
		
Better  solution use lambda function

	List<Apple> grrenApples=Inventrory2.filterApples(list, (Apple a)-> "green".equals(a.getColor()));

	(Apple a) -> a.getWeight() > 150
	(Apple a) -> a.getWeight() < 80 ||"brown".equals(a.getColor())
	
Even Better use method reference
	
	List<Apple>grrenApples=Inventrory2.filterApples(list, Inventrory2::isGreenApple);
	
#Streams API
	Streams API in Java 8 lets you write code that's
		-	Declarative More concise and readable
		-	Composable Greater flexibility
		-	Parallelizable Better performance

What are streams?
	
	-	Streams are an update to the Java API that lets you manipulate collections of data in a declarative way like what SQL Does to database!
	-	You can think of them as fancy iterators over a collection of data.
	-	In addition,streams can be processed in parallel transparently, without you having to	write any multithreaded code! (parallelization)

Why Streams?

	-	External iteration
	-	We don't have to manage iteration. Liberary is doing it for us
	-	Like 4GL(SQL like)
	-	Very easy to run on multicore systems

Sequential processing:
	
	List<Apple>heavyApples=list	.stream()
									.filter((Apple a)-> a.getWeight()>150)
									.collect(Collectors.toList());
Parallel processing:

	List<Apple> heavyApples =inventory.parallelStream().....;
	
Most important Predefined functional interface in Java 8 :

	Functional Interface | Description | Method Name | Use
	---------------------| ------------|-------------|---------------
	Predicate<T>         | T->boolean  | test()      | Used as filter
	Consumer<T>          | T-> void    | accept()    | Used in forEach
	Function<T,R>        | T->R        | apply()     | Used in map() operation
	Supplier<T>          | ()->T       | get()       |

Predicate<T>
	
	@FunctionalInterface
	public interface Predicate<T> {
	 	boolean test(T t);
		//...
	}



	Predicate<Integer>even=x->x%2==0;

	List<Integer>evens2=list.stream().filter(x->x%2==0).collect(Collectors.toList());

	Predicate<String> nonEmptyStringPredicate = (String s) -> !s.isEmpty();

	List<String> nonEmpty = filter(listOfStrings, nonEmptyStringPredicate);
	
Consumer<T>
	
	-	consumer interface defines an abstract method named accept that takes an object of generic type T and returns no result (void).
	-	You might use this interface when you need to access an object of type T and perform some operations on it. For example, you can use it to create a method forEach, which takes a list of Integers and applies an operation on each element of that list.
	
	@FunctionalInterface
	public interface Consumer<T>{
		void accept(T t);
	}
	
	public static<T> void forEach(List<T> list, Consumer<T> c){
		for(T i:list){
			c.accept(i);
		}
	}
	
	forEach(Arrays.asList(1,2,3,5),(Integer i) -> sysout(i));
	
Function<T,R>

	Mapping process: Accept object of one type map it to another type
	
	Ex: Want to print score of all players!
	
	public class Player {
		private String name;
		private String team;
		private int score;
		
		plist.stream()
			  .map(p-> p.getScore())
			  .forEach(x-> System.out.println(x));
		plist.stream()
			  .mapToInt(p-> p.getScore())
			  .forEach(x-> System.out.println(x));
	}

Reduction

	List<Integer>list=Arrays.asList(4,5,6,7,4,5,88);
	int sum=list.stream()
			     .reduce(0, (a,b)->a+b);
	System.out.println(sum);	

Collectors

	List<String>letter=Arrays.asList("a","b","c","d");
	String concat="";
	for(String temp:letter) {
		concat+=temp;
	}
		
	String concat2=letter.stream()
							.collect(Collectors.joining());

#Working with streams introduction
