package com.java8.groupBy_count_sort;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.java8.models.Item;

public class Stream_GroupBy_Count_Sort {
	
	public static void main(String[] args) {
		
		List<String> fruitsList = Arrays.asList("apple", "apple", "banana", "apple", "papaya", "orange", "banana", "papaya", "papaya");
		
		//Counting Elements
		System.out.println(fruitsList.stream()
										.count());
			
		//Grouping occurrence of elements in a List
		Map<String, Long> result = fruitsList.stream()
			  								 .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		
		//Sorting A Map
		Map<String, Long> finalMap = new LinkedHashMap<>();
		result.entrySet()
		 		.stream()
         		.sorted(Map.Entry.<String, Long>comparingByKey())
         		.forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));
		 
		//Iterating Map using stream
		finalMap.entrySet()
				.stream()
				.forEach(System.out::println);
				
		//Counting Elements of an Object
		List<Item> items = getItems();
		Map<String,Long> counting = items.stream()
											.collect(Collectors.groupingBy(Item::getName,Collectors.counting()));
		System.out.println(counting);
		
		//Summing field value of an Object
		Map<String,Integer> summing = items.stream()
											.collect(Collectors.groupingBy(Item::getName,Collectors.summingInt(Item::getQty)));
		System.out.println(summing);
		
		//Group By Element Of An Object 
		Map<BigDecimal, List<Item>> groupByPriceMap = items.stream()
				 											.collect(Collectors.groupingBy(Item::getPrice));
		System.out.println(groupByPriceMap);
		
		// Group by price, uses 'mapping' to convert List<Item> to Set<String>
		Map<BigDecimal, Set<String>> results = items.stream()
													.collect(Collectors.groupingBy(Item::getPrice, Collectors.mapping(Item::getName, Collectors.toSet())));
	    System.out.println(results);
		

	}

	private static List<Item> getItems() {
		List<Item> items = Arrays.asList(
                new Item("apple", 10, new BigDecimal("9.99")),
                new Item("banana", 20, new BigDecimal("19.99")),
                new Item("banana", 20, new BigDecimal("9.99")),
                new Item("orang", 10, new BigDecimal("29.99")),
                new Item("watermelon", 10, new BigDecimal("29.99")),
                new Item("papaya", 20, new BigDecimal("9.99")),
                new Item("apple", 10, new BigDecimal("9.99")),
                new Item("banana", 10, new BigDecimal("19.99")),
                new Item("apple", 20, new BigDecimal("9.99"))
        );
		
		return items;
	}
	
	


}
