package com.java8.map;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.java8.models.Developer;
import com.java8.models.Testers;

public class Stream_Map {
	
	public static void main(String[] args) {
		
		List<Developer> developers = getDevelopers();
		
		//Object To String Mapping
		developers.stream()
					.map(Developer::getfName)
					.forEach(System.out::println);
		
		List<Testers> testers = getTesters();
		
		//Object To Object Mapping
		List<Developer> newDev = testers.stream()
										.map(e->{
											Developer dev = new Developer();
											dev.setfName(e.getfName());
											dev.setlName(e.getlName());
											return dev;
										})
										.collect(Collectors.toList());
		
		newDev.forEach(System.out::println);
		
		
	}
	
	private static List<Developer> getDevelopers() {
		Developer dev1 = new Developer("Ram","Kumar");
		Developer dev2 = new Developer("Chandan","Kumar");
		Developer dev3 = new Developer("Gautam","Kumar");
		Developer dev4 = new Developer("Anand","Kumar");
		
		List<Developer> devlist = new ArrayList<>();
		devlist.add(dev1);
		devlist.add(dev2);
		devlist.add(dev3);
		devlist.add(dev4);
		return devlist;
	}
	
	private static List<Testers> getTesters() {
		Testers tester1 = new Testers("Kunal","Kundan");
		Testers tester2 = new Testers("Arav","Kumar");
		Testers tester3 = new Testers("Pranav","Prakash");
		Testers tester4 = new Testers("Dhruv","Pandey");
		
		List<Testers> testerList = new ArrayList<>();
		testerList.add(tester1);
		testerList.add(tester2);
		testerList.add(tester3);
		testerList.add(tester4);
		
		return testerList;
	}
		
}
