package com.java8.optional;

import java.util.Optional;

public class OptionalExample {
	
	/*
	 * Java 8 has introduced a new class Optional in java.util package. 
	 * It is used to represent a value is present or absent. 
	 * The main advantage of this new construct is that No more too many null checks and NullPointerException.
	 * It is also a Container to hold at most one value.
	 * 
	 * Optional.ofNullable() method returns a Non-empty Optional if a value present in the given object. Otherwise returns empty Optional.
	 * 
	 * Optional.empty() method is useful to create an empty Optional object.
	 * 
	 * Optional.isPresent() returns true if the given Optional object is non-empty. Otherwise it returns false.
	 * 
	 * Optional.ifPresent() performs given action if the given Optional object is non-empty. Otherwise it returns false.
	 *  
	 */
	
	public static void main(String[] args) {
		//Optional Basic example
		Optional<String> gender = Optional.of("MALE");
        Optional<String> emptyGender = Optional.empty();

		String answer1 = "Value";
        String answer2 = null;

        System.out.println("###################################Optional Basic example########################################");
        System.out.println("Non-Empty Optional:" + gender);
        System.out.println("Non-Empty Optional: Gender value : " + gender.get());
        System.out.println("Empty Optional: " + Optional.empty());

        System.out.println("Non-Empty Optional: " + Optional.ofNullable(answer1));
        System.out.println("Empty Optional: " + Optional.ofNullable(answer2));
                
        //Optional.map and flatMap
        System.out.println("###################################Optional.map and flatMap########################################");
        System.out.println("Non-Empty Optional:: " + gender.map(String::toUpperCase));
        System.out.println("Empty Optional    :: " + emptyGender.map(String::toUpperCase));

        Optional<Optional<String>> nonEmptyOtionalGender = Optional.of(Optional.of("male"));
        System.out.println("Optional value   :: " + nonEmptyOtionalGender);
        System.out.println("Optional.map     :: " + nonEmptyOtionalGender.map(g -> g.map(String::toUpperCase)));
        System.out.println("Optional.flatMap :: " + nonEmptyOtionalGender.flatMap(g -> g.map(String::toUpperCase)));
        
        //Optional.filter
        System.out.println("###################################Optional.filter########################################");
        System.out.println(gender.filter(g -> g.equals("male")));	//Optional.empty
	    System.out.println(gender.filter(g -> g.equalsIgnoreCase("MALE"))); //Optional[MALE]
	    System.out.println(emptyGender.filter(g -> g.equalsIgnoreCase("MALE"))); //Optional.empty

	    
	    //Optional isPresent and ifPresent
	    System.out.println("###################################Optional isPresent and ifPresent########################################");
        if (gender.isPresent()) {
            System.out.println("Value available.");
        } else {
            System.out.println("Value not available.");
        }
        gender.ifPresent(g -> System.out.println("In gender Option, value available."));
        emptyGender.ifPresent(g -> System.out.println("In emptyGender Option, value available."));

		//Optional orElse methods
        System.out.println("###################################Optional orElse methods########################################");
		System.out.println(gender.orElse("<N/A>")); //MALE
        System.out.println(emptyGender.orElse("<N/A>")); //<N/A>

        System.out.println(gender.orElseGet(() -> "<N/A>")); //MALE
        System.out.println(emptyGender.orElseGet(() -> "<N/A>")); //<N/A>
        
        
	}
}