package com.java8.list_to_map;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.java8.models.Developer;

public class StreamListToMap {
	public static void main(String[] args) {
		
		List<Developer> list = new ArrayList<>();
		list.add(new Developer("Ram", "Kumar"));
		list.add(new Developer("Shyam", "Chaturvedi"));
		list.add(new Developer("Kanwal", "Khurana"));
		list.add(new Developer("Madan", "Mohan"));
		list.add(new Developer("Chanchal", "Madan"));
			
		Map<String, String> result1 = list.stream()
        									.collect(Collectors.toMap(Developer::getfName, Developer::getlName));

		System.out.println(result1);
	}
}
