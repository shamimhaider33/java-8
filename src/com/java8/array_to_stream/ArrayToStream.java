package com.java8.array_to_stream;

import java.util.Arrays;
import java.util.stream.Stream;

public class ArrayToStream {
	public static void main(String[] args) {
		// In Java 8, you can either use Arrays.stream or Stream.of to convert an Array
		// into a Stream.
		String[] array = { "a", "b", "c", "d", "e" };

		// Arrays.stream
		Stream<String> stream1 = Arrays.stream(array);
		stream1.forEach(System.out::print);

		System.out.println("");

		// Stream.of
		Stream<String> stream2 = Stream.of(array);
		stream2.forEach(System.out::print);
	}
}
