package com.java8.stream_map_to_list;

import java.util.HashMap;
import java.util.Map;

public class StreamMapToList {
	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<>();
        map.put(10, "apple");
        map.put(20, "orange");
        map.put(30, "banana");
        map.put(40, "watermelon");
        map.put(50, "dragonfruit");
        
        map.entrySet()
        	.stream()
        	.map(x -> x.getKey() +" | "+ x.getValue())
        	.forEach(System.out::println);
        
	}
}
