package com.java8.already_opearated_or_closed_stream;

import java.util.Arrays;
import java.util.stream.Stream;

public class AlreadyOpearatedOrClosedStream {
	public static void main(String[] args) {
		/**
		 * Streams once operated or closed cannot be reused without 'Supplier'.
		 */
		String[] array = { "a", "b", "c", "d", "e" };

		Stream<String> stream = Arrays.stream(array);
		stream.forEach(x -> System.out.println(x));

		// reuse it to filter again! throws IllegalStateException
		long count = stream.filter(x -> "b".equals(x)).count();
		System.out.println(count);
	}
}
