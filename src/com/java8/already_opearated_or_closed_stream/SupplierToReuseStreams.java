package com.java8.already_opearated_or_closed_stream;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class SupplierToReuseStreams {

	public static void main(String[] args) {

		String[] array = { "a", "b", "c", "d", "e" };

		Supplier<Stream<String>> streamSupplier = () -> Stream.of(array);

		// get new stream
		streamSupplier.get().forEach(System.out::print);

		System.out.println("");

		// get another new stream
		long count = streamSupplier.get().filter(x -> "b".equals(x)).count();
		System.out.println(count);
	}
}
