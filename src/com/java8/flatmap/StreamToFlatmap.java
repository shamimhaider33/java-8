package com.java8.flatmap;

import java.util.Arrays;
import java.util.stream.Stream;

public class StreamToFlatmap {
	public static void main(String[] args) {
		
		/**
		 * In Java 8, Stream can hold different data types, for examples:
		 * 	Stream<String[]>	
		 *	Stream<Set<String>>	
		 *	Stream<List<String>>	
		 *	Stream<List<Object>>
		 *
		 * But, the Stream operations (filter, sum, distinctů) and collectors do not support it, so, we need flatMap() to do the following conversion :
		 *	Stream<String[]>		-> flatMap ->	Stream<String>
		 *	Stream<Set<String>>	-> flatMap ->	Stream<String>
		 *	Stream<List<String>>	-> flatMap ->	Stream<String>
		 *	Stream<List<Object>>	-> flatMap ->	Stream<Object>
		 *
		 * How flatMap() works :
		 *	{ {1,2}, {3,4}, {5,6} } -> flatMap -> {1,2,3,4,5,6}
		 *	{ {'a','b'}, {'c','d'}, {'e','f'} } -> flatMap -> {'a','b','c','d','e','f'}
		 *
		 */

		String[][] data = new String[][] { { "a", "b" }, { "c", "d" }, { "e", "f" } };

		Arrays.stream(data)
				.flatMap(Arrays::stream)
				.filter(x -> !"a".equals(x.toString()))
				.forEach(System.out::print);

		System.out.println("\n=========================================================");
		
		int[] intArray = { 1, 2, 3, 4, 5, 6 };

		Stream.of(intArray)
				.flatMapToInt(Arrays::stream)
				.forEach(System.out::print);

	}
}
