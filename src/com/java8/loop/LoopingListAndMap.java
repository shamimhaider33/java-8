package com.java8.loop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoopingListAndMap {
	public static void main(String[] args) {

		System.out.println("Map Items : ");
		Map<String, Integer> items = getItems();
		items.forEach((k, v) -> System.out.println("Key : " + k + " Value : " + v));

		System.out.println("\nList Items : ");
		List<String> itemsList = getItemsList();
		itemsList.forEach(System.out::println);

	}

	private static List<String> getItemsList() {
		List<String> itemsList = new ArrayList<>();
		itemsList.add("A");
		itemsList.add("B");
		itemsList.add("C");
		itemsList.add("D");
		itemsList.add("E");

		return itemsList;
	}

	private static Map<String, Integer> getItems() {
		Map<String, Integer> items = new HashMap<>();
		items.put("A", 10);
		items.put("B", 20);
		items.put("C", 30);
		items.put("D", 40);
		items.put("E", 50);
		items.put("F", 60);

		return items;
	}

}
