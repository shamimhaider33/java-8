package com.java8.collector_reducer;

import java.util.Arrays;
import java.util.List;

public class Reducer {
	public static void main(String[] args) {
		List<Integer>list=Arrays.asList(4,5,6,7,4,5,88);
		int sum=list .stream()
		             .reduce(1, (a,b)->a*b);
		System.out.println(sum);
	}
}
