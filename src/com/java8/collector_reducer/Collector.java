package com.java8.collector_reducer;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Collector {
	public static void main(String[] args) {
		List<String>letter=Arrays.asList("a","b","c","d");
		
		String concat2=letter.stream()
							 .collect(Collectors.joining());
		System.out.println(concat2);	//Output : abcd
	}

}
