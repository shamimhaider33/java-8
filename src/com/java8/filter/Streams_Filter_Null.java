package com.java8.filter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Streams_Filter_Null {
	public static void main(String[] args) {
		
		List<String> fruitsList = Arrays.asList("apple", null, "banana", "apple", null, "orange", "banana", "papaya", "papaya");

		// Filtering Null Values From List Of Objects
		fruitsList.stream()
					.filter(Objects::nonNull)
					.forEach(System.out::println);
	}
}
