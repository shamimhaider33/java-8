package com.java8.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.java8.models.Developer;

public class Streams_Filter_Collect {
	
	public static void main(String[] args) {
		
		List<Developer> developer = getDevelopers();

		//Filter And Collect
		List<Developer> fileteredDevelopers = developer.stream()
											.filter(e ->e.getlName().equals("Kumar"))
											.collect(Collectors.toList());

		fileteredDevelopers.forEach(System.out::println);
	}

	private static List<Developer> getDevelopers() {
		Developer dev1 = new Developer("Ram","Kumar");
		Developer dev2 = new Developer("Chandan","Chaman");
		Developer dev3 = new Developer("Gautam","Kumar");
		Developer dev4 = new Developer("Anand","Kaushik");

		List<Developer> devlist = new ArrayList<>();
		devlist.add(dev1);
		devlist.add(dev2);
		devlist.add(dev3);
		devlist.add(dev4);
		return devlist;
	}
	
	
}
