package com.java8.filter;

import java.util.ArrayList;
import java.util.List;

import com.java8.models.Developer;

public class Streams_Filter_FindAny_OrElse {

	public static void main(String[] args) {

		final List<Developer> developers = getDevelopers();

		Developer dev = developers.stream()
					.filter(e -> e.getfName().equals("Ram") || e.getlName().equals("Kumar"))
					.findAny()
					.orElse(null);

		System.out.println(dev);
	}
	
	private static List<Developer> getDevelopers() {
		Developer dev1 = new Developer("Ram","Kumar");
		Developer dev2 = new Developer("Chandan","Kumar");
		Developer dev3 = new Developer("Gautam","Kumar");
		Developer dev4 = new Developer("Anand","Kumar");

		List<Developer> devlist = new ArrayList<>();
		devlist.add(dev1);
		devlist.add(dev2);
		devlist.add(dev3);
		devlist.add(dev4);
		return devlist;
	}


}

