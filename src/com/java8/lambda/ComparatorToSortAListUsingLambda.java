package com.java8.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.java8.models.Developer;

public class ComparatorToSortAListUsingLambda {
	public static void main(String[] args) {
		
		List<Developer> devlist = getDevelopers();
		System.out.println("Not Sorted : "+ devlist);
		Collections.sort(devlist,legacyComparator);
		System.out.println("Legacy Sorted : "+ devlist);
		
		System.out.println("===============================================================");
		
		List<Developer> devlist2 = getDevelopers();
		System.out.println("Not Sorted : "+ devlist2);
		//Java 8, the List interface supports sort method directly, no need to use Collections.sort
		devlist2.sort(lambdaComparator);
		System.out.println("Lambda Sorted : "+ devlist2);
	}

	private static List<Developer> getDevelopers() {
		Developer dev1 = new Developer("Ram","Kumar");
		Developer dev2 = new Developer("Chandan","Kumar");
		Developer dev3 = new Developer("Gautam","Kumar");
		Developer dev4 = new Developer("Anand","Kumar");
		
		List<Developer> devlist = new ArrayList<>();
		devlist.add(dev1);
		devlist.add(dev2);
		devlist.add(dev3);
		devlist.add(dev4);
		return devlist;
	}

	//Legacy Comparator
	static final Comparator<Developer> legacyComparator = new  Comparator<Developer>() {
		@Override
		public int compare(Developer o1, Developer o2) {
			return o1.getfName().compareTo(o2.getfName());
		}
	};

	
	//Comparator using lambda
	static Comparator<Developer> lambdaComparator = (o1, o2) -> o1.getfName().compareTo(o2.getfName());

}
