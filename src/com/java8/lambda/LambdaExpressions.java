package com.java8.lambda;

import java.util.Arrays;
import java.util.List;

public class LambdaExpressions {
	public static void main(String[] args) {
		final List<String> count = Arrays.asList("One","Two");
		
		count.stream()
			.map(e -> e + " : ")				//.map() parameter is a simple lambda expression.
			.forEach(System.out::println);
	}
}
